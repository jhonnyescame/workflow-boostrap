var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');

/*
 * Variaveis
 */
// Sass caminho
var scssPasta = 'sass/style.scss';

// CSS destino 
var cssDest = 'css/';

// Options for development
var sassDevOptions = {
  outputStyle: 'expanded'
}

// Options for production
var sassProdOptions = {
  outputStyle: 'compressed'
}

/*
 * Tasks
 */
// Task 'sassdev' - Run with command 'gulp sassdev'
gulp.task('sassdev', function() {
  return gulp.src(scssPasta)
    .pipe(sass(sassDevOptions).on('error', sass.logError))
    .pipe(gulp.dest(cssDest));
});

// Task 'sassprod' - Run with command 'gulp sassprod'
gulp.task('sassprod', function() {
  return gulp.src(scssPasta)
    .pipe(sass(sassProdOptions).on('error', sass.logError))
    .pipe(rename('style.css'))
    .pipe(gulp.dest(cssDest));
});

// Task 'watch' - Run with command 'gulp watch'
gulp.task('watch', function() {
  gulp.watch(scssPasta, ['sassprod']);
  //gulp.watch(scssPasta, ['sassdev', 'sassprod']);
});

// Default task - Run with command 'gulp'
//gulp.task('default', ['sassdev', 'sassprod', 'watch']);
gulp.task('default', ['sassprod', 'watch']);
